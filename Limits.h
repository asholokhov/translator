#ifndef LIMITS_H
#define	LIMITS_H

// именное пространство ограничений диапазонов транслятора

namespace translator { namespace limits {
  
    // допустимый диапазон координат
    const int MIN_X = 0;
    const int MIN_Y = 0;
    
    const int MAX_X = 10;
    const int MAX_Y = 20;
    
    // допустимый диапазон пропуска ходов
    const int MIN_SKIP_COUNT = 1;
    const int MAX_SKIP_COUNT = 20;
    
    // допустимый диапазон скорости игры
    // т.к скорость измеряется временем таймера - т.е задержкой,
    // минимальная скорость должна быть больше, чем максимальная
    // чем больше значение, тем медленней скорость игры
    const int MIN_SPEED = 1000;
    const int MAX_SPEED = 100;
    
}}

#endif	/* LIMITS_H */

