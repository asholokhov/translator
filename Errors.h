#ifndef ERRORS_H
#define	ERRORS_H

#include "ErrorHandler.h"

// именное пространство управления ошибками в транлсяторе

namespace translator { namespace errors {

    // лог ошибок
    ErrorHandler error_log;
    
}}

#endif	/* ERRORS_H */

