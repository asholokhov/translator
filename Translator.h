#ifndef TRANSLATOR_H
#define	TRANSLATOR_H

#include <set>
#include <string>
#include <sstream>

using namespace std;

// именное пространство транслятора

namespace translator {

    // наборы ключевых слов, типов фигур и движений
    set <string> keywords;
    set <string> figure_types;
    set <string> move_types;
    
    // выходной код транслятора
    ostringstream code_flow;

    // инициализация транслятора
    void init_translator() {
	// добавление ключевых слов
	keywords.insert("create");
	keywords.insert("skip");
	keywords.insert("move");
	keywords.insert("setspeed");
	keywords.insert("rotate");

	// добавление типов фигур
	figure_types.insert("square");
	figure_types.insert("squiggle");
	figure_types.insert("rsquiggle");
	figure_types.insert("lblock");
	figure_types.insert("rlblock");
	figure_types.insert("tblock");
	figure_types.insert("linepiece");

	// добавление типов движений
	move_types.insert("left");
	move_types.insert("right");
	move_types.insert("force");
    }

}

#endif	/* TRANSLATOR_H */

