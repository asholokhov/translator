#ifndef ERRORHANDLER_H
#define	ERRORHANDLER_H

#include <string>
#include <vector>
#include <iostream>

using namespace std;

enum EErrorTypes {
    ET_SYNTAX,
    ET_RANGE,
    ET_INV_TYPE,
    ET_WARNING,
    ET_UNKNOWN,
    ET_NO_ERROR,
    ET_MULTIPLE_SETSPEED_DEF,
    ET_INV_TYPE_EXP_INTEGER,
    ET_INV_TYPE_EXP_FIGURE,
    ET_INV_TYPE_EXP_MOVE,
    ET_NEGATIVE_VALUE,
    ET_CANT_READ_INPUT_FILE,
    ET_CANT_WRITE_OUTPUT_FILE
};

class ErrorHandler {
public:

    void add_error(string msg, EErrorTypes err_type, int line) {
	m_err_list.push_back(error(msg, err_type, line));
    };

    bool has_errors() const {
	return m_err_list.size() != 0;
    }

    void print_error_list() {
	for (int i = 0; i < m_err_list.size(); i++) {
	    cout << "Line " << m_err_list[i].line << ": ";
	    switch (m_err_list[i].type) {
		case ET_SYNTAX:
		    cout << "syntax error in statement ";
		    break;

		case ET_RANGE:
		    cout << "invalid range of statement ";
		    break;

		case ET_INV_TYPE:
		    cout << "invalid type of statement ";
		    break;

		case ET_WARNING:
		    cout << "warning in statement ";
		    break;

		case ET_UNKNOWN:
		    cout << "unknown error - ";
		    break;
                    
                case ET_MULTIPLE_SETSPEED_DEF:
                    cout << "multiple definition of command, you can use it only one time - ";
                    break;
                    
                case ET_INV_TYPE_EXP_INTEGER:
                    cout << "invalid type of statement, expected integer but found - ";
                    break;
                    
                case ET_INV_TYPE_EXP_FIGURE:
                    cout << "invalid type of statement, expected figure type but found - ";
                    break;
                    
                case ET_INV_TYPE_EXP_MOVE:
                    cout << "invalid type of statement, expected move type but found - ";
                    break;
                    
                case ET_NEGATIVE_VALUE:
                    cout << "expected positive value, but found negative - ";
                    break;
                    
                case ET_CANT_READ_INPUT_FILE:
                    cout << "cant read input file, please, check file and your permissions";
                    break;
                    
                case ET_CANT_WRITE_OUTPUT_FILE:
                    cout << "cant write output file, please, check your permissions";
                    break;
	    }
	    cout << "`" << m_err_list[i].message << "`" << endl;
	}
    }

private:
    struct error {
	string message;
	int line;
	EErrorTypes type;

	error() : message(""), type(ET_UNKNOWN), line(-1)
	{}

	error(string msg, EErrorTypes e_type, int line_pos)
		: message(msg), type(e_type), line(line_pos)
	{}
    };

    vector <error> m_err_list;
};

#endif	/* ERRORHANDLER_H */