// стандартные библиотеки С++
#include <cstdlib>
#include <fstream>
#include <string>
#include <iterator>
#include <sstream>
#include <iostream>
#include <algorithm>

// заголовочные файлы транслятора
#include "Checkers.h"
#include "Errors.h"
#include "CodeTemplates.h"
#include "Translator.h"
#include "Limits.h"

// определения файлов
#define STD_IN_FILE  "demo.in"
#define STD_OUT_FILE "demoscene.h"

using namespace std;

int main(int argc, char** argv) {
    // входной файл
    ifstream in(STD_IN_FILE);

    // инициализация транслятора
    translator::init_translator();

    // текущая позиция транслятора
    int current_line = 0;
    string strLine;
    
    // команда skip может использоваться только один раз
    int setspeed_found_count = 0;

    // заголовок выходного файла
    translator::code_flow << translator::code_template::get_initial_code();
    
    // проверка входного файла
    if (!in.is_open())
        translator::errors::error_log.add_error(STD_IN_FILE, ET_CANT_READ_INPUT_FILE, 0);

    // начало процесса трансляции
    while (getline(in, strLine)) {
	// считываем следующую строку из файла
	istringstream istream(strLine);
	current_line++;

	string token;
	while (!istream.eof()) {
	    // получаем следующее выражение из строки до пробела
	    istream >> token;

	    // проверяем выражение
	    // если это ключевое слово, обрабатываем его. иначе - синтаксическая ошибка
	    if (translator::checkers::syntax::is_keyword(token)) {

		// если требуется создать фигуру
		if (token == "create") {
		    // проверяем параметры комманды
		    // получаем тип создаваемой фигуры
		    string figure;
		    istream >> figure;

		    // проверяем на корректность типа новой фигуры
		    if (translator::checkers::types::is_figure(figure)) {

			// получаем координаты, где создавать фигуру
			string str_x, str_y;
			istream >> str_x >> str_y;

			// проверка типа координат 
			bool is_num_x = translator::checkers::numeric::is_number(str_x);
			bool is_num_y = translator::checkers::numeric::is_number(str_y);

			// если тип координат корректный
			if (is_num_x && is_num_y) {
			    
			    // проверяем координаты на корректный диапазон значений
			    int x = stoi(str_x);
			    int y = stoi(str_y);
                            
                            if (x < 0) {
                                translator::errors::error_log.add_error(str_x, ET_NEGATIVE_VALUE, current_line);
                            }
                            
                            if (y < 0) {
                                translator::errors::error_log.add_error(str_y, ET_NEGATIVE_VALUE, current_line);
                            }

			    bool range_x = translator::checkers::ranges::in_range(x,
				    translator::limits::MIN_X, translator::limits::MAX_X);
			    bool range_y = translator::checkers::ranges::in_range(y,
				    translator::limits::MIN_Y, translator::limits::MAX_Y);

			    // если диапазон границ корректен
			    if (range_x && range_y) {
				// формируем код комманды create
				translator::code_flow << translator::code_template::get_create_code(figure, x, y);
			    } else {
				// если диапазон не корретен, сообщаем об ошибке диапазонов
				if (!range_x) {
				    translator::errors::error_log.add_error(str_x, ET_RANGE, current_line);
				}

				if (!range_y) {
				    translator::errors::error_log.add_error(str_y, ET_RANGE, current_line);
				}
			    }

			} else {
			    // если координаты не являются числом, сообщаем о несоответсвии типов
			    if (!is_num_x) {
				translator::errors::error_log.add_error(str_x, ET_INV_TYPE_EXP_INTEGER, current_line);
			    }

			    if (!is_num_y) {
				translator::errors::error_log.add_error(str_y, ET_INV_TYPE_EXP_INTEGER, current_line);
			    }
			}

		    } else {
			// недопустимый тип фигуры, сообщаем об ошибке
			translator::errors::error_log.add_error(figure, ET_INV_TYPE_EXP_FIGURE, current_line);

			// пропускаем следующие два выражения, т.к комманда create требует 3 аргумента
			istream >> figure;
			istream >> figure;
		    }
		}

		// если требуется пропустить ходы
		if (token == "skip") {
                    // считываем параметр
		    string steps_str;
		    istream >> steps_str;
                    
		    // проверяем, является ли параметр числом
		    if (translator::checkers::numeric::is_number(steps_str)) {
			
			// параметр - число, проверяем диапазон
			int steps = stoi(steps_str);
                        
                        if (steps < 0) {
                            translator::errors::error_log.add_error(steps_str, ET_NEGATIVE_VALUE, current_line);
                        }
                        
			bool range_steps = translator::checkers::ranges::in_range(steps,
				translator::limits::MIN_SKIP_COUNT, translator::limits::MAX_SKIP_COUNT);

			// если диапазон корректный
			if (range_steps) {
			    // формируем код для пропуска ходов
			    translator::code_flow << translator::code_template::get_skip_code(steps);
			} else {
			    // сообщаем об ошибке диапазона для количества пропусков
			    translator::errors::error_log.add_error(steps_str, ET_RANGE, current_line);
			}

		    } else {
			// параметр не является числом - несоответствие типов
			translator::errors::error_log.add_error(steps_str, ET_INV_TYPE_EXP_INTEGER, current_line);
		    }
		}

		// если требуется передвинуть фигуру
		if (token == "move") {
		    // считаем направление движения
		    string mv_action;
		    istream >> mv_action;

		    // проверим параметр на принадлежность к типу движений
		    if (translator::checkers::types::is_move(mv_action)) {
			// если тип совпадает, формируем код для передвижения фигуры 
			translator::code_flow << translator::code_template::get_move_code(mv_action);
		    } else {
			// сообщаем об ошибке несоотвествия типов
			translator::errors::error_log.add_error(mv_action, ET_INV_TYPE_EXP_MOVE, current_line);
		    }
		}

		// если требуется установить скорость игры
		if (token == "setspeed") {
		    // считаем параметр
		    string speed_str;
		    istream >> speed_str;
                    
                    // нашли set_speed
                    setspeed_found_count++;
                    
                    // если больше одной комманды, сообщаем об ошибке
                    if (setspeed_found_count > 1) {
                        translator::errors::error_log.add_error(token, ET_MULTIPLE_SETSPEED_DEF, current_line);
                        
                        // продолжаем трансляцию
                        continue;
                    }

		    // проверим, является ли параметр числовым
		    if (translator::checkers::numeric::is_number(speed_str)) {
			
			// если параметр числовой, проверим диапазон
			int speed = stoi(speed_str);
                        
                        if (speed < 0) {
                            translator::errors::error_log.add_error(speed_str, ET_NEGATIVE_VALUE, current_line);
                        }
                        
			bool range_speed = translator::checkers::ranges::in_range(speed,
				translator::limits::MAX_SPEED, translator::limits::MIN_SPEED);

			if (range_speed) {
			    // если диапазон скорости корректный, формируем код для установки нужной скорости
			    translator::code_flow << translator::code_template::get_setspeed_code(speed);
			} else {
			    // сообщаем о недопустимости диапазона для скорости
			    translator::errors::error_log.add_error(speed_str, ET_RANGE, current_line);
			}

		    } else {
			// параметр не числового типа, сообщаем об ошибке несоответсвия типов
			translator::errors::error_log.add_error(speed_str, ET_INV_TYPE_EXP_INTEGER, current_line);
		    }
		}

		// требуется повернуть фигуру
		if (token == "rotate") {
		    // комманда без параметров, формируем код поворота
		    translator::code_flow << translator::code_template::get_rotate_code();
		}

	    } else {
		// сообщаем о синтаксической ошибке во время трансляции
		translator::errors::error_log.add_error(token, ET_SYNTAX, current_line);
	    }

	}
    }

    // если трансляция закончилась без ошибок
    if (!translator::errors::error_log.has_errors()) {
	// сообщаем об этом пользователю
	cout << "Translation was successful." << endl;
	
	// формируем завершающий код
	translator::code_flow << translator::code_template::get_complete_code();
	
	// сохраняем выходной файл
	ofstream out(STD_OUT_FILE);
        
        // проверяем выходной файл
        if (!out.is_open()) {
            translator::errors::error_log.add_error(STD_OUT_FILE, ET_CANT_WRITE_OUTPUT_FILE, current_line);
            translator::errors::error_log.print_error_list();
        } else
            out << translator::code_flow.str();
        
	out.close();
    } else {
	// сообщаем пользователю об ошибках трансляции
	cout << "Translation was unsuccessful. Errors list:" << endl;
	translator::errors::error_log.print_error_list();
    }

    return 0;
}