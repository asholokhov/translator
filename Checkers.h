#ifndef CHECKERS_H
#define	CHECKERS_H

#include <string>
using std::string;

#include "Translator.h"

// именное пространство всех проверок в трансляторе

namespace translator { namespace checkers {

	// проверка типов
	namespace types {
	    
	    // тип фигуры
	    inline bool is_figure(const string &token) {
		return translator::figure_types.find(token) != translator::figure_types.end();
	    }

	    // тип движения
	    inline bool is_move(const string &token) {
		return translator::move_types.find(token) != translator::move_types.end();
	    }
	}

	// числовые проверки
	namespace numeric {

	    // является ли выражение числом
	    bool is_number(const string &token) {
		int offset = 0;
		
		if (token.length() > 0 && token[0] == '-')
		    offset = 1;

		return ((!token.empty()) && (find_if(token.begin() + offset, token.end(), [](char ch) {
		    return !std::isdigit(ch);
		})) == token.end());
	    }
	}

	// проверка диапазона
	namespace ranges {

	    // проверка вхождения x в диапазон [from_r; to_r]
	    inline bool in_range(const int x, const int from_r, const int to_r) {
		return (x >= from_r && x <= to_r);
	    }
	}

	// синтаксические проверки
	namespace syntax {

	    // является ли выражение ключевым словом
	    inline bool is_keyword(const string &token) {
		return translator::keywords.find(token) != translator::keywords.end();
	    }
	}

}}

#endif	/* CHECKERS_H */